import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocketFactory;

public class BuzzStreamAPIClient {

	public static void main (String[] Args){

		String host = "api.buzzstream.com";
		String path = "/v1/links";
		String consumerKey = "2334a3f7-cd3d-4e0f-b4d9-cad0c8fc2505";
		String consumerSecret = "3fJMu9hwiUL-CmanqZbWBS2KhBDwhMjChdcmJNeZoLh-FtrzB7S_rLrEk684z9yp";
		int numAdditionalParameters = 0;
		String[] additionalParameters = new String[numAdditionalParameters];
		
		OAuthAuthenticator authenticator = new OAuthAuthenticator(host, path, consumerKey, consumerSecret);
		
		Socket connection = null;
		BufferedReader in = null;
		OutputStream out = null;
		try{
			connection = SSLSocketFactory.getDefault().createSocket( host,  443);
			in = new BufferedReader(new InputStreamReader( connection.getInputStream() ) );
			out = connection.getOutputStream();
		} catch (IOException e){
			System.out.print("Could not create Buffered Reader");
		} catch (Exception e){
			System.out.println("Could not open connection");
			System.exit(0);
		}
		
		// Build Request
		String requestLine = "GET " + path;
	    
		if ( additionalParameters.length > 0 )
			for ( int i = 0; i < additionalParameters.length; i++ ){
				requestLine += ( ( i == 0 ) ? "?" : "&" ) + additionalParameters[ i ];
			}
		
		// Send Request
		requestLine += " HTTP/1.1";
		try{
			String oauthHeader = authenticator.generateOauthHeader("GET", additionalParameters );
			
		    out.write( ( requestLine + "\r\n" ).getBytes() );
		    out.write( ( "Host: " + host + "\r\n" ).getBytes() );
		    out.write( ( "Connection: Close\r\n" ).getBytes() );
		    out.write( ( oauthHeader + "\r\n" ).getBytes() );
		    out.write( "\r\n".getBytes() );
		} catch (IOException e){
			System.out.println("Could not send request");
			System.exit(0);
		}
		
		// Receive Response
		String responseHeader = null;
		String[] responseHeaderComponents = null;
		try{
			responseHeader = in.readLine();
		    responseHeaderComponents = responseHeader.split( " " );
		    if ( responseHeaderComponents.length < 3 )
		    {
		      // XXX you probably want to do more robust error handling here...
		      throw new IOException( "Malformed server response: " + responseHeader );
		    }
		} catch (IOException e){
			System.out.println("Could not receive response");
			System.exit(0);
		}
		
		if ( !( "200".equals( responseHeaderComponents[ 1 ] ) ) ) {
			System.out.println("Problem with Response");
			System.out.println( responseHeader );
			//System.exit(0);
	    }

	    // Just skip the headers, don't care.
		try{
			while ( in.readLine().trim().length() > 0 );
		} catch (IOException e){
			System.out.println("Could not skip headers");
			System.exit(0);
		}
	    
	    String responseLine = null;
	    try{
	    	responseLine = in.readLine();
	    } catch (IOException e){
	    	System.out.print("Could not read response line after headers");
	    	System.exit(0);
	    }
	    
	    String[] responseNvPairs = responseLine.split( "&" );
	    for ( String responseNvPair : responseNvPairs ){
	    	System.out.println( responseNvPair );
	    }
	}
	
}