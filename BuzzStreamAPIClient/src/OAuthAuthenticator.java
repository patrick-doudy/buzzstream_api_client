import java.util.Collections;
import java.util.ArrayList;
import java.util.Date;
import java.util.Base64;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OAuthAuthenticator {

	private static final String UTF8 = StandardCharsets.UTF_8.name();
	private String host;
	private String path;
	private String consumerKey;
	private String consumerSecret;
	
	public OAuthAuthenticator( String host, String path, String consumerKey, String consumerSecret){
		this.host = host;
		this.path = path;
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
	}
	
	public String generateOauthHeader( String method, String[] additionalParameters){
		long timestamp = new Date().getTime() / 1000;
		String nonce = Long.toString(timestamp); // TODO: Change to random long
		
		ArrayList<String> parameters = new ArrayList<String>();
		parameters.add("oauth_consumer_key=" + consumerKey);
		parameters.add("oauth_nonce=" + nonce);
		parameters.add("oauth_signature_method=HMAC-SHA1");
		parameters.add("oauth_timestamp=" + timestamp);
		parameters.add("oauth_version=1.0");
		
		for( String additionalParameter : additionalParameters){
			parameters.add(additionalParameter);
		}
		
		Collections.sort(parameters);
		
		StringBuffer parametersList = new StringBuffer();
		
		// use ternary operator to concatenate parameters with '&'
		for(int i=0;i<parameters.size();i++){
			parametersList.append( ((i>0) ? "&" : "") + parameters.get(i));
		}
		
		// construct signature base string
		String signatureBaseString = null;
		try{
		signatureBaseString = method + "&"
				+ URLEncoder.encode( "https://" + host + path , UTF8 ) + "&"
				+ URLEncoder.encode( parametersList.toString(), UTF8 );
		} catch (UnsupportedEncodingException e){
			// Encoding Exception really shouldn't happen
		}
		
		// generate signature
		String signature = null;
		Base64.Encoder base64 = Base64.getEncoder();
		try{
			SecretKeySpec signingKey = new SecretKeySpec( (consumerSecret+"&").getBytes(), "HmacSHA1");
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);
			byte[] rawHMAC = mac.doFinal( signatureBaseString.getBytes() );
			signature = base64.encodeToString( rawHMAC );
		} catch (Exception e){
			System.out.println("Unable to append signature "+e.toString());
			System.exit(0);
		}
		
		String authorizationLine = null;
		try{
		authorizationLine =
				"Authorization: OAuth "+
				"oauth_consumer_key=\"" + consumerKey + "\", " +
				"oauth_nonce=\"" + nonce + "\", " +
				"oauth_signature=\"" + URLEncoder.encode( signature, UTF8) + "\", " +
				"oauth_signature_method=\"" + "HMAC-SHA1" + "\", " +
				"oauth_timestamp=\"" + timestamp + "\", " +
				"oauth_version=\"" + "1.0" +"\"";
		} catch (UnsupportedEncodingException e){
			// really shouldn't happen
		}
		
		return authorizationLine;
	}
	
}
